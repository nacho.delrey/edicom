# Curso de kuberenetes (k8s) usando minikube
Comprende 4 días de curso

1.- Conceptos básicos + Ejemplos
2.- Namespaces + RS, Deployments, Pods, límitar recursos, Config-maps, secrets, volumes
3.- Networking (Cluster-IP, NodePort, Ingress -> Traefik), HealthChecks y Autoscaling (Horizontal Pod Autoscaling)
4.- Helm, Gitlab y ETCD

```
├── README.md
├── day1
│   └── nginx.yml
├── day2
│   ├── deployment.yaml
│   ├── limitrange.yaml
│   ├── local-storage.yaml
│   ├── mariadb-deployment-configmap.yaml
│   ├── mariadb-deployment-secret.yaml
│   ├── mariadb-deployment.yaml
│   ├── namespace.yaml
│   ├── nginx.yaml
│   ├── pv-claim.yaml
│   ├── pv-pod.yaml
│   ├── pv-volume.yaml
│   ├── replicaset.yaml
│   ├── resourcequotas.yaml
│   ├── service_ci.yaml
│   ├── service_np.yaml
│   ├── sharevol.yaml
│   └── variables-de-entorno
│       ├── mariadb-deployment-configmap.yaml
│       ├── mariadb-deployment-secret.yaml
│       └── mariadb-deployment.yaml
├── day3
│   ├── autoscaling
│   │   ├── deployment.yaml
│   │   └── hpa-autoscaling.yaml
│   ├── ejemplo1
│   │   ├── nginx-deployment.yaml
│   │   ├── nginx-ingress.yaml
│   │   └── nginx-svc.yaml
│   ├── ejemplo2
│   │   ├── animals-deployment.yaml
│   │   ├── animals-ingress-2.yaml
│   │   ├── animals-ingress.yaml
│   │   └── animals-svc.yaml
│   ├── healthcheck
│   │   ├── badpod.yaml
│   │   ├── pod.yaml
│   │   └── ready.yaml
│   ├── networking
│   │   ├── nginx-deployment.yaml
│   │   ├── service_clusterip.yaml
│   │   └── service_nodeport.yaml
│   └── traefik
│       ├── traefik-ds.yaml
│       └── traefik-rbac.yaml
└── day4
```

